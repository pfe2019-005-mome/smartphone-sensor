import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_socket_io/socket_io_manager.dart';
import 'package:sensors/sensors.dart';
import 'package:flutter_socket_io/flutter_socket_io.dart';

class Exercise extends StatefulWidget {
  @override
  _ExerciseState createState() => _ExerciseState();
}

class _ExerciseState extends State<Exercise> {
  String _gyroscopeJSON;
  List<StreamSubscription<dynamic>> _streamSubscriptions =
      <StreamSubscription<dynamic>>[];
  String dropdownValue = 'no-sensor';
  List<String> items = <String>[
      'no-sensor',
      'on-sensor-shoulder',
      'on-sensor-elbow',
      'on-sensor-wrist',
      'on-sensor-sword'
  ];

  SocketIO socketIO;

  @override
  void initState() {
    super.initState();

    socketIO = SocketIOManager().createSocketIO("http://192.168.43.211:3000", "/");
    socketIO.init();
    socketIO.connect();

//    //Accelerometer events
//    _streamSubscriptions
//        .add(accelerometerEvents.listen((AccelerometerEvent event) {
//      setState(() {
//        _accelerometerValues = <double>[event.x, event.y, event.z];
//      });
//    }));
//
//    //UserAccelerometer events
//    _streamSubscriptions
//        .add(userAccelerometerEvents.listen((UserAccelerometerEvent event) {
//      setState(() {
//        _userAccelerometerValues = <double>[event.x, event.y, event.z];
//      });
//    }));

    //Gyroscope events
    _streamSubscriptions.add(gyroscopeEvents.listen((GyroscopeEvent event) {
      setState(() {
        if (dropdownValue != 'no-sensor') {
          _gyroscopeJSON = '{' +
              '"timestamp":"' + (new DateTime.now().microsecondsSinceEpoch).toString() + '",' +
              '"gyro": {"x":"'+event.x.toString()+'","y":"'+event.y.toString()+'","z":"'+event.z.toString()+'"}' + ',' +
              '"accel": {"x":"'+event.x.toString()+'","y":"'+event.y.toString()+'","z":"'+event.z.toString()+'"}' + ',' +
              '"fusionPose": {"x":"'+event.x.toString()+'","y":"'+event.y.toString()+'","z":"'+event.z.toString()+'"}' + ',' +
              '"compass": {"x":"'+event.x.toString()+'","y":"'+event.y.toString()+'","z":"'+event.z.toString()+'"}' +
              '}';
          socketIO.sendMessage(dropdownValue, _gyroscopeJSON);
        }
      });
    }));
  }

  @override
  void dispose() {
    for (StreamSubscription<dynamic> sub in _streamSubscriptions) {
      sub.cancel();
    }
    socketIO.disconnect();
    SocketIOManager().destroySocket(socketIO);

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
        new Container(
            padding: EdgeInsets.only(bottom: 15.0),
            width: 200,
            height: 65,
            child: DropdownButton<String>(
                  value: dropdownValue,
                  icon: Icon(Icons.arrow_downward),
                  iconSize: 24,
                  elevation: 16,
                  style: TextStyle(
                      color: Colors.blue
                  ),
                  underline: Container(
                    height: 2,
                    color: Colors.blue,
                  ),
                  onChanged: (String newValue) {
                    setState(() {
                      dropdownValue = newValue;
                    });
                  },
                  items: items
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),
            ),
          ],
        ),
      ),
    );
  }
}