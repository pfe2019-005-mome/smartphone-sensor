import 'package:flutter/material.dart';
import 'package:smartphone_sensor/exercice_menu.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Smartphone sensor',
      theme: new ThemeData(primarySwatch: Colors.teal),
      home: new ExerciseMenu(),
    );
  }
}